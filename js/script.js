/*
Author: Able Sense Media
Web: ablesense.com
Date of creation: 2014/01/01
*/

var APP = (function () {
	var me = {},
		browser = {}

	/////////////////////////////////////////////////////////////////
	////////////////////// PRIVATE FUNCTIONS ////////////////////////
	/////////////////////////////////////////////////////////////////
		//private vars
		;

	function getSVGsupport() {
		return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1");
	}

	function getMQsupport() {
		return (typeof window.matchMedia == 'function');
	}

	function isTouch() {
		return 'ontouchstart' in window || 'onmsgesturechange' in window;
	}

	function getAnimationSupport() {
		var b = document.body || document.documentElement,
			s = b.style,
			p = 'animation';

		if (typeof s[p] == 'string') { return true; }

		// Tests for vendor specific prop
		var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
		p = p.charAt(0).toUpperCase() + p.substr(1);

		for (var i=0; i<v.length; i++) {
			if (typeof s[v[i] + p] == 'string') { return true; }
		}

		return false;
	}

	function getFlexboxSupport() {
		var props = ['-webkit-flex', '-ms-flexbox', 'flex'],
			len = props.length,
			detect = document.createElement('div'),
			supported = false;

		while(len-- && !supported) {
			var val = props[len];
			detect.style.display = val;
			supported = (detect.style.display === val);
		}

		return supported;
	}

	function getNthChildSupport() {
		// selectorSupported lovingly lifted from the mad italian genius, Diego Perini
		// http://javascript.nwbox.com/CSSSupport/

		var support,
			sheet,
			doc = document,
			root = doc.documentElement,
			head = root.getElementsByTagName('head')[0],
			impl = doc.implementation || {
				hasFeature: function() {
					return false;
				}
			},
			selector = ':nth-child(2n+1)',
			link = doc.createElement("style");

		link.type = 'text/css';

		(head || root).insertBefore(link, (head || root).firstChild);

		sheet = link.sheet || link.styleSheet;

		if (!(sheet && selector)) return false;

		support = impl.hasFeature('CSS2', '') ?

		function(selector) {
			try {
				sheet.insertRule(selector + '{ }', 0);
				sheet.deleteRule(sheet.cssRules.length - 1);
			} catch (e) {
				return false;
			}
			return true;
		} : function(selector) {
			sheet.cssText = selector + ' { }';
			return sheet.cssText.length !== 0 && !(/unknown/i).test(sheet.cssText) && sheet.cssText.indexOf(selector) === 0;
		};

		return support(selector);
	}

	function getViewportSize() {
		browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	}

	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			}, wait);
			if (immediate && !timeout) func.apply(context, args);
		};
	}

	/////////////////////////////////////////////////////////////////
	////////////////////// PUBLIC FUNCTIONS /////////////////////////
	/////////////////////////////////////////////////////////////////

	me.cancelEvent = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
	};

	me.onResize = function(callback) {
		callback();

		$(window).on('resize', debounce(function() {
			callback();
		}, 200));
	};


	browser.supportsAnimation = getAnimationSupport();
	browser.supportsSVG = getSVGsupport();
	browser.supportsMQ = getMQsupport();
	browser.supportsNthChild = getNthChildSupport();
	browser.supportsFlexbox = getFlexboxSupport();
	browser.viewportSize = getViewportSize();
	browser.isTouch = isTouch();
	browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

	me.onResize(getViewportSize);

	me.browser = browser;

	return me;
})();

(function() {
	$('html').removeClass('no-js');

	if (!APP.browser.supportsSVG) {
		$('html').addClass('no-svg');

		$('img').each(	function(n){
			var src = n.src;
			n.src = src.replace('.svg', '.png');
		});
	}

	if (!APP.browser.supportsMQ) {
		var respond = document.createElement('script');
		respond.src = '/js/respond.js';
		document.body.appendChild(respond);
	}

	if (!APP.browser.supportsNthChild) {
		//Test for nth-child support and add .clearrow class when not supported
	}

})();

!function() {
	// Create a ul element inside the body and give it an id of #ul
	document.write('<ul id="ul">');
	// Create a variable named ul and assign it to the element with the id #ul
	var ul = document.getElementById('ul'),
	// Also create a function named _ with two parameters; html, inside
		_ = function(html, inside) {
			// for loop: create variable i and assign it the value of 0
			// while i is less than the length of the array inside run the loop
			// and each time the loop runs add 1 to i
			for (var i = 0; i < inside.length; i++) {
				console.log(i); // log the loop count
				// Write the following inside of the ul element
				ul.innerHTML += '<' + html + '>' + inside[i] + '</' + html + '>';
			}
		};

	var JSON = {
		"listItem": "li",
		"address": {
			"streetAddress": "21 2nd Street streetAddress",
			"postalCode": "l5m 3w9"
		},
		"phoneNumbers": [
			"902 903 9939",
			"555 555 5555"
		]
	};
	console.log(JSON);

	_(JSON.listItem, JSON.address.streetAddress);
	_(JSON.listItem, JSON.phoneNumbers);
	_(JSON.listItem, JSON.address.postalCode.toUpperCase());
}();